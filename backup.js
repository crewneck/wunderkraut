var taskInput = document.getElementById("new-task");
var list = document.getElementById("list");
var listItems = document.getElementsByTagName("LABEL");


if(listItems) 



function addTodo() {

	var listItem = document.createElement("li");
	var checkBox = document.createElement("input");
	var label = document.createElement("label");

	checkBox.type = "checkbox";

	label.innerText = taskInput.value;

	label.setAttribute("contenteditable", "true");


	listItem.appendChild(checkBox);
	listItem.appendChild(label);

	list.appendChild(listItem);


	taskInput.value = "";

}





taskInput.addEventListener("focus", focusBackground);

function focusBackground() {
	taskInput.style.backgroundColor = "#E7F1CC";
}


taskInput.addEventListener("blur", blurBackground);

function blurBackground() {
	taskInput.style.backgroundColor = "inherit";
}



taskInput.addEventListener("keypress", keyPressed);  

function keyPressed(k) {
	if (k.code == 'Enter')      
		addTodo();               
	return false;             
}



var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LABEL') {
    ev.target.classList.toggle('checked');
    ev.target.setAttribute("contenteditable", "true");
  }
}, false);
