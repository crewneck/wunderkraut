var taskInput = document.getElementById("new-task");
var list = document.getElementById("list");
var listItems = document.getElementsByTagName("LABEL");
var checkBox = document.querySelector("input[type=checkbox]");
var remove = document.getElementById("remove");



function addTodo() {

	var listItem = document.createElement("li");
	var checkBox = document.createElement("input");
	var label = document.createElement("label");
	var liInner = document.createElement("div");
	var x = document.createElement("span");

	checkBox.type = "checkbox";

	label.innerText = taskInput.value;

	label.contenteditable = "true";

	liInner.className = "li-inner";

	x.innerText = "x";

	liInner.appendChild(checkBox);
	liInner.appendChild(label);
	liInner.appendChild(x);

	listItem.appendChild(liInner);

	list.appendChild(listItem);

	taskInput.value = "";

	for (i = 0; i < listItems.length; i++) {
		listItems[i].setAttribute("contenteditable", "true");
	}

	checkBox.addEventListener("click", test);

	x.addEventListener("click", crossRemove);

	remove.addEventListener("click", deleteList);

}

function keyPressed(k) {
	if (k.code == 'Enter')
		addTodo();
	return false;
}

function test() {

	var complete = this;
	if (this.checked === false) {
		complete.nextElementSibling.setAttribute("contenteditable", "true");
		complete.nextElementSibling.style.textDecoration = "none";
		complete.nextElementSibling.nextElementSibling.style.display = "none";
		
		
	}
	else {
		complete.nextElementSibling.setAttribute("contenteditable", "false");
		complete.nextElementSibling.style.textDecoration = "line-through";
		complete.nextElementSibling.nextElementSibling.style.display = "block";
	}

}


function crossRemove() {
	var cross = this;
	var node = this.parentNode;
	node.remove();
}


function deleteList() {
	list.innerHTML= "";
}



taskInput.addEventListener("focus", focusBackground);

function focusBackground() {
	taskInput.style.backgroundColor = "#E7F1CC";
}


taskInput.addEventListener("blur", blurBackground);

function blurBackground() {
	taskInput.style.backgroundColor = "inherit";
}


taskInput.addEventListener("keypress", keyPressed);







